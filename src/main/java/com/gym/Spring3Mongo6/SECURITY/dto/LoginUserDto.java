package com.gym.Spring3Mongo6.SECURITY.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LoginUserDto {
    @NotBlank(message= " Username es necesario")
    private String Username;

    @NotBlank(message= " Password es necesario")
    private String Password;
}
