package com.gym.Spring3Mongo6.SECURITY.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CreateUserDto {
    @NotBlank(message= " Username es necesario")
    private String Username;

    @NotBlank(message= " Mail es necesario")
    @Email(message = "el Mail es invalido")
    private String email;

    @NotBlank(message= " Password es necesario")
    private String Password;

    //    @NotEmpty(message = "Rol es necesario")
    List<String> roles = new ArrayList<>();

}
