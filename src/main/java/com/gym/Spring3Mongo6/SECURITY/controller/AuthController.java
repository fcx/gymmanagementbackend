package com.gym.Spring3Mongo6.SECURITY.controller;

import com.gym.Spring3Mongo6.GLOBAL.dto.MessageDto;
import com.gym.Spring3Mongo6.GLOBAL.exceptions.AttributeException;
import com.gym.Spring3Mongo6.SECURITY.dto.CreateUserDto;
import com.gym.Spring3Mongo6.SECURITY.dto.JwtTokenDto;
import com.gym.Spring3Mongo6.SECURITY.entity.UserEntity;
import com.gym.Spring3Mongo6.SECURITY.service.UserEntityService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.gym.Spring3Mongo6.SECURITY.dto.LoginUserDto;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {

    UserEntityService userEntityService;

    public AuthController(UserEntityService userEntityService) {
        this.userEntityService = userEntityService;
    }

    @PostMapping("/create")
    public ResponseEntity<MessageDto> create(@Valid @RequestBody CreateUserDto dto) throws AttributeException {
        UserEntity userEntity = userEntityService.create(dto);
        return ResponseEntity.ok(new MessageDto(HttpStatus.OK, "user " + userEntity.getUsername() + " have been created"));
    }

    @PostMapping("/create-admin")
    public ResponseEntity<MessageDto> createAdmin(@Valid @RequestBody CreateUserDto dto) throws AttributeException {
        UserEntity userEntity = userEntityService.createAdmin(dto);
        return ResponseEntity.ok(new MessageDto(HttpStatus.OK, "Admin " + userEntity.getUsername() + " have been created"));
    }

    @PostMapping("/create-user")
    public ResponseEntity<MessageDto> createUser(@Valid @RequestBody CreateUserDto dto) throws AttributeException {
        UserEntity userEntity = userEntityService.createAdmin(dto);
        return ResponseEntity.ok(new MessageDto(HttpStatus.OK, "user " + userEntity.getUsername() + " have been created"));
    }

    @PostMapping("/login")
    public ResponseEntity<JwtTokenDto> login(@Valid @RequestBody LoginUserDto dto) throws AttributeException {
        JwtTokenDto jwtTokenDto = userEntityService.login(dto);
        return ResponseEntity.ok(jwtTokenDto);
    }
}
