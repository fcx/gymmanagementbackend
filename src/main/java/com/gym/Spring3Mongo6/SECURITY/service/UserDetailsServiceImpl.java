package com.gym.Spring3Mongo6.SECURITY.service;

import com.gym.Spring3Mongo6.SECURITY.entity.UserEntity;
import com.gym.Spring3Mongo6.SECURITY.repository.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserEntityRepository userEntityRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserEntity> userEntity = userEntityRepo.findByUsernameOrEmail(username, username);
        if(userEntity.isEmpty()){
            throw new UsernameNotFoundException("usuario no existe");
        }
        return UserPrincipal.build(userEntity.get());
    }


}
