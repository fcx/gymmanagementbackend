package com.gym.Spring3Mongo6.SECURITY.service;

import com.gym.Spring3Mongo6.GLOBAL.exceptions.AttributeException;
import com.gym.Spring3Mongo6.GLOBAL.utils.Operations;
import com.gym.Spring3Mongo6.SECURITY.dto.CreateUserDto;
import com.gym.Spring3Mongo6.SECURITY.dto.JwtTokenDto;
import com.gym.Spring3Mongo6.SECURITY.dto.LoginUserDto;
import com.gym.Spring3Mongo6.SECURITY.entity.UserEntity;
import com.gym.Spring3Mongo6.SECURITY.enums.RoleEnum;
import com.gym.Spring3Mongo6.SECURITY.jwt.JwtProvider;
import com.gym.Spring3Mongo6.SECURITY.repository.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserEntityService {
    @Autowired
    UserEntityRepository userEntityRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    public UserEntity create(CreateUserDto dto) throws AttributeException {
        if(userEntityRepo.existsByUsername(dto.getUsername()))
            throw new AttributeException("username already in use");
        if(userEntityRepo.existsByEmail(dto.getEmail()))
            throw new AttributeException("email already in use");
        if(dto.getRoles().isEmpty())
            throw new AttributeException("roles are mandatory");
        return userEntityRepo.save(mapUserFromDto(dto));
    }

    public JwtTokenDto login(LoginUserDto dto) {
        Authentication authentication =
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtProvider.generateToken(authentication);
        return new JwtTokenDto(token);
    }



    public UserEntity createAdmin(CreateUserDto dto) throws AttributeException {
        if(userEntityRepo.existsByUsername(dto.getUsername()))
            throw new AttributeException("username already in use");
        if(userEntityRepo.existsByEmail(dto.getEmail()))
            throw new AttributeException("email already in use");
        List<String> roles = Arrays.asList("ROLE_ADMIN","ROLE_USER");
        dto.setRoles(roles);
        return userEntityRepo.save(mapUserFromDto(dto));
    }

    public UserEntity createUser(CreateUserDto dto) throws AttributeException {
        if(userEntityRepo.existsByUsername(dto.getUsername()))
            throw new AttributeException("username already in use");
        if(userEntityRepo.existsByEmail(dto.getEmail()))
            throw new AttributeException("email already in use");
        List<String> roles = List.of("ROLE_USER");
        dto.setRoles(roles);
        return userEntityRepo.save(mapUserFromDto(dto));
    }


    //Private methods
    private UserEntity mapUserFromDto(CreateUserDto dto){
        int id = Operations.autoIncrement(userEntityRepo.findAll());

        String password = passwordEncoder.encode(dto.getPassword());

        List<RoleEnum> roles =
                dto.getRoles().stream().map(RoleEnum::valueOf).collect(Collectors.toList());

        return new UserEntity(id , dto.getUsername(), dto.getEmail(), password, roles);
    }
}
