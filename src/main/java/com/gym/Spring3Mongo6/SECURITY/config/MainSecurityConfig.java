package com.gym.Spring3Mongo6.SECURITY.config;

import com.gym.Spring3Mongo6.SECURITY.dto.CreateUserDto;
import com.gym.Spring3Mongo6.SECURITY.dto.JwtTokenDto;
import com.gym.Spring3Mongo6.SECURITY.dto.LoginUserDto;
import com.gym.Spring3Mongo6.SECURITY.entity.UserEntity;
import com.gym.Spring3Mongo6.SECURITY.jwt.JwtEntryPoint;
import com.gym.Spring3Mongo6.SECURITY.jwt.JwtFilter;
import com.gym.Spring3Mongo6.SECURITY.service.UserDetailsServiceImpl;
import com.gym.Spring3Mongo6.SECURITY.service.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
public class MainSecurityConfig {

    @Autowired
    UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtEntryPoint jwtEntryPoint;

    @Autowired
    JwtFilter jwtFilter;

    AuthenticationManager authenticationManager;


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        AuthenticationManagerBuilder builder = http.getSharedObject(AuthenticationManagerBuilder.class);
        builder.userDetailsService(userDetailsServiceImpl).passwordEncoder(passwordEncoder);
        authenticationManager = builder.build();
        http.authenticationManager(authenticationManager);
        http.csrf().disable();
        http.cors();
        http.authorizeHttpRequests().requestMatchers("/auth/**","/swagger-ui/**","/v3/api-docs/**").permitAll().anyRequest().authenticated();
        http.exceptionHandling().authenticationEntryPoint(jwtEntryPoint);
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
        return http.build();

    }
    @Bean
    public CommandLineRunner run(UserEntityService userService) {
        return args -> {
            LoginUserDto dto = new LoginUserDto();
            dto.setUsername("fuser");
            dto.setPassword("fuser");

            JwtTokenDto jwtToken = userService.login(dto);
            System.out.println("Logged in user: " + dto.getUsername());
            System.out.print("JWT token: " + jwtToken.getToken());
        };
    }



}
