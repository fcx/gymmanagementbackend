package com.gym.Spring3Mongo6.SECURITY.entity;

import com.gym.Spring3Mongo6.GLOBAL.entities.EntityId;
import com.gym.Spring3Mongo6.SECURITY.enums.RoleEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Document(collection = "users")
public class UserEntity extends EntityId {
    private String username;
    private String password;
    private String email;
    List<RoleEnum> roles;

    public UserEntity(int id, String username, String email, String password, List<RoleEnum> roles) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }
    @Override
    public int getId(){
        return super.getId();
    }

    @Override
    public void setId(int id){
        super.setId(id);
    }
}
