package com.gym.Spring3Mongo6.SECURITY.enums;

public enum RoleEnum {
    ROLE_ADMIN, ROLE_USER;
}
