package com.gym.Spring3Mongo6.CRUD.controller;

import com.gym.Spring3Mongo6.CRUD.dto.ClienteDto;
import com.gym.Spring3Mongo6.CRUD.entity.Cliente;
import com.gym.Spring3Mongo6.CRUD.entity.PlanSuscripcion;
import com.gym.Spring3Mongo6.CRUD.service.ClienteService;
import com.gym.Spring3Mongo6.GLOBAL.dto.MessageDto;
import com.gym.Spring3Mongo6.GLOBAL.entities.Paquete;
import com.gym.Spring3Mongo6.GLOBAL.exceptions.AttributeException;
import com.gym.Spring3Mongo6.GLOBAL.exceptions.ResourceNotFoundException;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    ClienteService clienteService;

    public ClienteController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }


    @GetMapping
    public ResponseEntity<List<Cliente>> getAll(){
        // Obtener el userID del usuario autenticado
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        return ResponseEntity.ok(clienteService.getAll(userId));
    }
    @GetMapping("/{id}")
    public ResponseEntity<Cliente> getById(@PathVariable("id") int id) throws ResourceNotFoundException {
        // Obtener el userID del usuario autenticado
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        return ResponseEntity.ok(clienteService.getById(id));
    }

    @PostMapping
    public ResponseEntity<MessageDto> save(@Valid @RequestBody ClienteDto dto, Authentication authentication) throws AttributeException {
        String userID = authentication.getName();
        int durationMonths = dto.getPlanSuscripcion().getDurationMonths();
        Paquete paquete = dto.getPlanSuscripcion().getPaquete();

        PlanSuscripcion plan = new PlanSuscripcion(durationMonths, paquete);
        Cliente cliente = clienteService.save(dto, userID, plan);

        String message = "Cliente " + cliente.getNombre() + " " + cliente.getApellido() + " creado correctamente";
        return ResponseEntity.ok(new MessageDto(HttpStatus.OK, message));
    }

    @PutMapping("/{id}")
    public ResponseEntity<MessageDto> update(@PathVariable("id") int id, @Valid @RequestBody ClienteDto dto) throws ResourceNotFoundException, AttributeException {
        // Obtener el userID del usuario autenticado
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();

        Cliente cliente = clienteService.update(id, dto, userId);
        String message = "Cliente " + cliente.getNombre() + " actualizado correctamente";
        return ResponseEntity.ok(new MessageDto(HttpStatus.OK, message));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<MessageDto> delete(@PathVariable("id") int id) throws ResourceNotFoundException {
        // Obtener el userID del usuario autenticado
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();

        Cliente cliente = clienteService.delete(id, userId);
        String message = "Cliente " + cliente.getNombre() + " eliminado correctamente";
        return ResponseEntity.ok(new MessageDto(HttpStatus.OK, message));
    }

}
