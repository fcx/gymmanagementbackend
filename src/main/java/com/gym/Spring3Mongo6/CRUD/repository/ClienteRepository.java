package com.gym.Spring3Mongo6.CRUD.repository;

import ch.qos.logback.core.net.server.Client;
import com.gym.Spring3Mongo6.CRUD.entity.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteRepository extends MongoRepository<Cliente,Integer> {
    boolean existsByEmail(String email);
    List<Cliente> findByUserID(String userID);


//    @Query("{'userID': ?0, 'id': ?1}")
//    Optional<Cliente> findClienteByUserIDAndId(String userID, Integer id);
}
