package com.gym.Spring3Mongo6.CRUD.entity;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.gym.Spring3Mongo6.GLOBAL.entities.Paquete;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor

public class PlanSuscripcion {
    private int durationMonths;
    private Paquete paquete;

    public PlanSuscripcion(int durationMonths, Paquete paquete) {
        if (durationMonths <= 0) {
            throw new IllegalArgumentException("Duration months must be greater than 0");
        }
        if (paquete == null) {
            throw new IllegalArgumentException("Paquete cannot be null");
        }
        this.durationMonths = durationMonths;
        this.paquete = paquete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlanSuscripcion that = (PlanSuscripcion) o;
        return durationMonths == that.durationMonths &&
                Objects.equals(paquete, that.paquete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(durationMonths, paquete);
    }
    @Override
    public String toString() {
        return "PlanSuscripcion{" +
                "durationMonths=" + durationMonths +
                ", paquete=" + paquete +
                '}';
    }


}
