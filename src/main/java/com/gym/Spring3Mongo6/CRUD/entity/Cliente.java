package com.gym.Spring3Mongo6.CRUD.entity;


import com.gym.Spring3Mongo6.GLOBAL.entities.EntityId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "clientes")
public class Cliente extends EntityId {
    private String userID; // id del usuario autenticado
    private String nombre;
    private String apellido;
    private int numero;
    private int edad;

    private String email;
    private boolean necesitaEntrenador;
    private Date fechaSuscripcion;
    private PlanSuscripcion planSuscripcion;
    private Date fechaVencimiento;

    public Cliente(int id, String userID, String nombre, String apellido, int numero, int edad, String email, boolean necesitaEntrenador, Date fechaSuscripcion, PlanSuscripcion planSuscripcion) {
        this.id = id;
        this.userID = userID;
        this.nombre = nombre;
        this.apellido = apellido;
        this.numero = numero;
        this.edad = edad;
        this.email = email;
        this.necesitaEntrenador = necesitaEntrenador;
        this.fechaSuscripcion = fechaSuscripcion;
        this.planSuscripcion = planSuscripcion;
        this.fechaVencimiento = calculateEndDate(fechaSuscripcion, planSuscripcion);
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public int getId() {
        return super.getId();
    }


    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setPlanSuscripcion(PlanSuscripcion planSuscripcion) {
        this.planSuscripcion = planSuscripcion;
        this.fechaVencimiento = calculateEndDate(fechaSuscripcion, planSuscripcion);
    }

    private Date calculateEndDate(Date fechaSuscripcion, PlanSuscripcion planSuscripcion) {
        LocalDate localDate = fechaSuscripcion.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = localDate.plusMonths(planSuscripcion.getDurationMonths());
        return Date.from(endDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    @Override
    public String toString() {
        return "Cliente{" + "userID='" + userID + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", numero=" + numero +
                ", edad=" + edad +
                ", email='" + email + '\'' +
                ", necesitaEntrenador=" + necesitaEntrenador +
                ", fechaSuscripcion=" + fechaSuscripcion +
                ", planSuscripcion=" + planSuscripcion +
                ", fechaVencimiento=" + fechaVencimiento +
                ", id=" + id +
                '}';
    }
}
