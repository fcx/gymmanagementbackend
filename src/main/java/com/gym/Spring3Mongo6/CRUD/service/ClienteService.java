package com.gym.Spring3Mongo6.CRUD.service;


import com.gym.Spring3Mongo6.CRUD.dto.ClienteDto;
import com.gym.Spring3Mongo6.CRUD.entity.Cliente;
import com.gym.Spring3Mongo6.CRUD.entity.PlanSuscripcion;
import com.gym.Spring3Mongo6.CRUD.repository.ClienteRepository;
import com.gym.Spring3Mongo6.GLOBAL.exceptions.ResourceNotFoundException;
import com.gym.Spring3Mongo6.GLOBAL.utils.Operations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteService {

    private final ClienteRepository clienteRepository;

    @Autowired
    public ClienteService(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    public List<Cliente> getAll(String userID){
        return clienteRepository.findByUserID(userID);
    }
    public Cliente getById(int id) throws ResourceNotFoundException {
        return clienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cliente no encontrado")); // falta testear esto y revisarlo para que solo consiga los id del usuario
    }

    public Cliente save(ClienteDto dto, String userID, PlanSuscripcion planSuscripcion) {
        int id = Operations.autoIncrement(clienteRepository.findAll());
        Cliente cliente = new Cliente(id, userID, dto.getNombre(), dto.getApellido(), dto.getNumero(), dto.getEdad(), dto.getEmail(), dto.isNecesitaEntrenador(), dto.getFechaSuscripcion(), planSuscripcion);
        cliente.setUserID(userID);
        return clienteRepository.save(cliente);
    }


    public Cliente update(int id, ClienteDto dto, String userId) throws ResourceNotFoundException {
        Cliente cliente = clienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cliente no encontrado"));
        cliente.setNombre(dto.getNombre());
        cliente.setApellido(dto.getApellido());
        cliente.setNumero(dto.getNumero());
        cliente.setEdad(dto.getEdad());
        cliente.setEmail(dto.getEmail());
        cliente.setNecesitaEntrenador(dto.isNecesitaEntrenador());
        cliente.setUserID(userId);
        return clienteRepository.save(cliente);
    }
    public Cliente delete(int id, String userId) throws ResourceNotFoundException {
        Cliente cliente = clienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cliente no encontrado"));
        if (!cliente.getUserID().equals(userId)) {
            throw new ResourceNotFoundException("No se puede eliminar el cliente");
        }
        clienteRepository.delete(cliente);
        return cliente;
    }
}
