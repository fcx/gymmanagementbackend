package com.gym.Spring3Mongo6.CRUD.dto;

import com.gym.Spring3Mongo6.CRUD.entity.PlanSuscripcion;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.NumberFormat;

import java.util.Date;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClienteDto {
    @NotBlank(message = "El usuario no puede estar vacío")
    private String userID;
    @NotBlank(message = "El nombre no puede estar vacío")
    private String nombre;
    @NotBlank(message = "El apellido no puede estar vacío")
    private String apellido;
    @NumberFormat(pattern = "##########")
    private int numero;

    @Min(value = 13, message = "La edad debe ser mayor a 13 años")
    private int edad;
    @NotBlank(message = "El email no puede estar vacío")
    @Email
    private String email;

    @NotNull
    private boolean necesitaEntrenador;

    @NotNull
    private Date fechaSuscripcion;


    private PlanSuscripcion planSuscripcion;

    public boolean isNecesitaEntrenador() {
        return necesitaEntrenador;
    }

    public void setNecesitaEntrenador(boolean necesitaEntrenador) {
        this.necesitaEntrenador = necesitaEntrenador;
    }

    public PlanSuscripcion getPlanSuscripcion() {
        return planSuscripcion;
    }

    public void setPlanSuscripcion(PlanSuscripcion planSuscripcion) {
        this.planSuscripcion = planSuscripcion;
    }
}
