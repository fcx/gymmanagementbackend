package com.gym.Spring3Mongo6.GLOBAL.utils;


import com.gym.Spring3Mongo6.GLOBAL.entities.EntityId;

import java.util.List;
import java.util.*;

public class Operations {
    public static int autoIncrement(List<? extends EntityId> list){
        if(list.isEmpty())
            return 1;
        return list.stream().max(Comparator.comparing(EntityId::getId)).get().getId() + 1;
    }
}

