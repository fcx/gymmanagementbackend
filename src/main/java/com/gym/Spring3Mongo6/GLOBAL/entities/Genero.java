package com.gym.Spring3Mongo6.GLOBAL.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Genero {

      MASCULINO("Masculino"),
      FEMENINO("Femenino"),
      OTRO("Prefiere no Decirlo");

      @Getter
      private final String Genero;
}
