package com.gym.Spring3Mongo6.GLOBAL.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Paquete {

    MENSUAL("Mensual", 1),
    TRES_MESES("3Meses", 3),
    SEIS_MESES("6Meses", 6),
    ANUAL("Anual", 12);

    private final String name;
    private final int value;

}
