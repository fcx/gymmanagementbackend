package com.gym.Spring3Mongo6.CRUD.entity;

import com.gym.Spring3Mongo6.GLOBAL.entities.Paquete;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

class ClienteTest {


    private Cliente cliente;

    @BeforeEach
    public void setUp() {
        int id = 1;
        String userID = "user123";
        String nombre = "John";
        String apellido = "Doe";
        int numero = 123;
        int edad = 30;
        String email = "john.doe@example.com";
        boolean necesitaEntrenador = true;
        LocalDate fechaSuscripcion = LocalDate.of(2023, 5, 1);
        PlanSuscripcion planSuscripcion = new PlanSuscripcion(3, Paquete.TRES_MESES);

        cliente = new Cliente(id, userID, nombre, apellido, numero, edad, email, necesitaEntrenador, Date.from(fechaSuscripcion.atStartOfDay(ZoneId.systemDefault()).toInstant()), planSuscripcion);
    }

    @Test
    public void testConstructor() {
        assertEquals(1, cliente.getId());
        assertEquals("user123", cliente.getUserID());
        assertEquals("John", cliente.getNombre());
        assertEquals("Doe", cliente.getApellido());
        assertEquals(123, cliente.getNumero());
        assertEquals(30, cliente.getEdad());
        assertEquals("john.doe@example.com", cliente.getEmail());
        assertTrue(cliente.isNecesitaEntrenador());
        assertEquals(Date.from(LocalDate.of(2023, 5, 1).atStartOfDay(ZoneId.systemDefault()).toInstant()), cliente.getFechaSuscripcion());
        assertEquals(new PlanSuscripcion(3, Paquete.TRES_MESES), cliente.getPlanSuscripcion());
    }

    @Test
    public void testEndDateCalculation() {
        LocalDate expectedEndDate = LocalDate.of(2023, 8, 1);
        Date expectedEndDateAsDate = Date.from(expectedEndDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        assertEquals(expectedEndDateAsDate, cliente.getFechaVencimiento());
    }

//    @Test test innecesario
//    public void testEndDateCalculationForTwoClients() {
//        // Arrange and ACT
//        LocalDate fechaSuscripcion1 = LocalDate.of(2023, 5, 1);
//        PlanSuscripcion planSuscripcion1 = new PlanSuscripcion(3, Paquete.TRES_MESES);
//        Cliente cliente1 = new Cliente(4, "userID1", "nombre1", "apellido1", 123, 22, "email1@gmail.com", true, Date.from(fechaSuscripcion1.atStartOfDay(ZoneId.systemDefault()).toInstant()), planSuscripcion1);
//        System.out.println(cliente1.toString());
//
//        LocalDate fechaSuscripcion2 = LocalDate.of(2023, 5, 1);
//        PlanSuscripcion planSuscripcion2 = new PlanSuscripcion(6, Paquete.MENSUAL);
//        Cliente cliente2 = new Cliente(5, "userID2", "nombre2", "apellido2", 123, 22, "email1@gmail.com", false, Date.from(fechaSuscripcion2.atStartOfDay(ZoneId.systemDefault()).toInstant()), planSuscripcion2);
//        System.out.println(cliente2.toString());
//
//        // Assert
//        LocalDate expectedEndDate1 = fechaSuscripcion1.plusMonths(planSuscripcion1.getDurationMonths());
//        Date expectedEndDateAsDate1 = Date.from(expectedEndDate1.atStartOfDay(ZoneId.systemDefault()).toInstant());
//        assertEquals(expectedEndDateAsDate1, cliente1.getFechaVencimiento());
//
//        LocalDate expectedEndDate2 = fechaSuscripcion2.plusMonths(planSuscripcion2.getDurationMonths());
//        Date expectedEndDateAsDate2 = Date.from(expectedEndDate2.atStartOfDay(ZoneId.systemDefault()).toInstant());
//        assertEquals(expectedEndDateAsDate2, cliente2.getFechaVencimiento());
//    }


}