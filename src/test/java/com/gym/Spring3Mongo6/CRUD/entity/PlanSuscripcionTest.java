package com.gym.Spring3Mongo6.CRUD.entity;

import com.gym.Spring3Mongo6.GLOBAL.entities.Paquete;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlanSuscripcionTest {

    @Test
    public void testConstructorAndGetters() {
        PlanSuscripcion plan = new PlanSuscripcion(3, Paquete.TRES_MESES);
        assertEquals(3, plan.getDurationMonths());
        assertEquals(Paquete.TRES_MESES, plan.getPaquete());
    }

    @Test
    public void testSetters() {
        PlanSuscripcion plan = new PlanSuscripcion(1, Paquete.MENSUAL);
        plan.setDurationMonths(6);
        assertEquals(6, plan.getDurationMonths());

        plan.setPaquete(Paquete.ANUAL);
        assertEquals(Paquete.ANUAL, plan.getPaquete());
    }

    @Test
    public void testToString() {
        PlanSuscripcion planSuscripcion = new PlanSuscripcion(1, Paquete.MENSUAL);
        assertNotNull(planSuscripcion.toString());
    }

    @Test
    public void testValidInput() {
        PlanSuscripcion plan = new PlanSuscripcion(6, Paquete.ANUAL);
        assertEquals(6, plan.getDurationMonths());
        assertEquals(Paquete.ANUAL, plan.getPaquete());
    }

    @Test
    public void testConstructorWithInvalidArguments() {
        assertThrows(IllegalArgumentException.class, () -> new PlanSuscripcion(0, Paquete.MENSUAL));
        assertThrows(IllegalArgumentException.class, () -> new PlanSuscripcion(-1, Paquete.ANUAL));
        assertThrows(IllegalArgumentException.class, () -> new PlanSuscripcion(12, null));
    }

    @Test
    public void testInvalidDuration() {
        assertThrows(IllegalArgumentException.class, () -> new PlanSuscripcion(-1, Paquete.MENSUAL));
    }
}