package com.gym.Spring3Mongo6.CRUD.service;

import com.gym.Spring3Mongo6.CRUD.dto.ClienteDto;
import com.gym.Spring3Mongo6.CRUD.entity.Cliente;
import com.gym.Spring3Mongo6.CRUD.entity.PlanSuscripcion;
import com.gym.Spring3Mongo6.CRUD.repository.ClienteRepository;
import com.gym.Spring3Mongo6.GLOBAL.entities.Paquete;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ClienteServiceTest {

    private ClienteService underTest;
    @Mock
    private ClienteRepository clienteRepository;
    String userID = "fuser";

    @BeforeEach
    void setUp() {
        underTest = new ClienteService(clienteRepository);
    }



    @Test
    void getAll(){
        //GIVEN
        underTest.getAll(userID);
        //WHEN
        verify(clienteRepository).findByUserID(userID);
        //THEN
//        Assertions.assertNotNull(underTest.getAll(userID));
        Assertions.assertEquals(0, underTest.getAll(userID).size());
    }

    @Test
    void saveCliente() {
        // GIVEN
        ClienteDto dto = new ClienteDto();
        dto.setNombre("Juan");
        dto.setApellido("Perez");
        dto.setNumero(123456789);
        dto.setEdad(30);
        dto.setEmail("juan.perez@example.com");
        dto.setNecesitaEntrenador(false);
        Date fechaSuscripcion = new Date();
        dto.setFechaSuscripcion(fechaSuscripcion);
        PlanSuscripcion plan = new PlanSuscripcion(1, Paquete.MENSUAL);

        Cliente cliente = new Cliente(1, userID, dto.getNombre(), dto.getApellido(), dto.getNumero(), dto.getEdad(), dto.getEmail(), dto.isNecesitaEntrenador(), dto.getFechaSuscripcion(), plan);

        Mockito.when(clienteRepository.findAll()).thenReturn(Collections.emptyList());
        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).thenReturn(cliente);

        // WHEN
        Cliente savedCliente = underTest.save(dto, userID, plan);

        // THEN
        Mockito.verify(clienteRepository, Mockito.times(1)).save(Mockito.any(Cliente.class));
        Assertions.assertNotNull(savedCliente);
        Assertions.assertEquals(userID, savedCliente.getUserID());
        Assertions.assertEquals(dto.getNombre(), savedCliente.getNombre());
        Assertions.assertEquals(dto.getApellido(), savedCliente.getApellido());
        Assertions.assertEquals(dto.getNumero(), savedCliente.getNumero());
        Assertions.assertEquals(dto.getEdad(), savedCliente.getEdad());
        Assertions.assertEquals(dto.getEmail(), savedCliente.getEmail());
        Assertions.assertEquals(dto.isNecesitaEntrenador(), savedCliente.isNecesitaEntrenador());
        Assertions.assertEquals(dto.getFechaSuscripcion(), savedCliente.getFechaSuscripcion());
        Assertions.assertEquals(plan, savedCliente.getPlanSuscripcion());
        Assertions.assertNotNull(savedCliente.getFechaVencimiento());

        // Verify that the fechaVencimiento property is calculated correctly
        Date expectedFechaVencimiento = calculateEndDate(fechaSuscripcion, plan);
        System.out.println(expectedFechaVencimiento.toString());
        System.out.println(savedCliente.getFechaVencimiento().toString());
        Assertions.assertEquals(expectedFechaVencimiento, savedCliente.getFechaVencimiento());
    }

    private Date calculateEndDate(Date fechaSuscripcion, PlanSuscripcion planSuscripcion) {
        LocalDate localDate = fechaSuscripcion.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = localDate.plusMonths(planSuscripcion.getDurationMonths());
        return Date.from(endDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

}